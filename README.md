# OpenML dataset: KDD98

https://www.openml.org/d/23513

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset KDD98 challenge: https://kdd.ics.uci.edu/databases/kddcup98/kddcup98.html
      
      The goal is to estimate the return from a direct mailing in order to maximize donation profits.
      This dataset represents problem of binary classification - whether there was a response to mailing.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/23513) of an [OpenML dataset](https://www.openml.org/d/23513). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/23513/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/23513/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/23513/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

